#!/bin/bash -e

if [ $# -eq 0 ]; then
    echo "No arguments provided"
	echo "run.sh scan_folder mask_file output_folder project_id session_id session_label scan_id mask_file_uri mask_roi_matcher"
    exit 1
fi

die() {
    echo "@" 2>&1
    exit 1
}

scan="$1"
shift
mask="$1"
shift
output="$1"
shift
project="$1"
shift
session_id="$1"
shift
session_label="$1"
shift
scan_id="$1"
shift
mask_file_uri="$1"
shift
mask_roi_matcher="$1"

echo "Scan file: ${scan}"
echo "Mask file: ${mask}"
echo "Output dir: ${output}"
echo "Project: ${project}"
echo "Session ID: ${session_id}"
echo "Session label: ${session_label}"
echo "Scan ID: ${scan_id}"
echo "Mask File URI: ${mask_file_uri}"
echo "Mask ROI Matcher: ${mask_roi_matcher}"

if [[ $mask = *" "* ]]
then
	echo "Mask name: \"${mask}\" may not contain spaces."
	echo "Halting execution."
	exit 1
fi;

if [ -z "${mask_roi_matcher}"]
then
	mask_roi_matcher="*.nrrd"
	echo "No Mask ROI Matcher supplied. Matching all ${mask_roi_matcher}."
else
	mask_roi_matcher="*${mask_roi_matcher}*.nrrd"
	echo "Matching ROIs with label: ${mask_roi_matcher}"
fi;

echo "Convert DICOM to NRRD using plastimatch"
mkdir -p /output/NRRD/DICOM
mkdir -p /output/NRRD/MASK

plastimatch convert --input ${scan} --output-img ${output}/NRRD/DICOM/${scan_id}.nrrd

plastimatch convert --input ${mask} --output-prefix ${output}/NRRD/MASK --prefix-format nrrd --output-ss-list ${output}/NRRD/MASK/RTSS.txt --fixed ${output}/NRRD/DICOM/${scan_id}.nrrd

echo "Scan Header: ${output}/NRRD/DICOM/${scan_id}.nrrd"
plastimatch header ${output}/NRRD/DICOM/${scan_id}.nrrd
echo "Mask Header: ${output}/NRRD/MASK/*"
plastimatch header ${output}/NRRD/MASK/*.nrrd

# Begin constructing CSV batch file
batch_file="/output/batch.csv"
echo "Image,Mask" > ${batch_file}
for roi in ${output}/NRRD/MASK/${mask_roi_matcher}; do
	echo  "Added: ${output}/NRRD/DICOM/${scan_id}.nrrd,${roi} to batch processing list."
	echo  "${output}/NRRD/DICOM/${scan_id}.nrrd,${roi}" >> ${batch_file}
done

echo
echo "Running pyradiomics"

echo "pyradiomics ${batch_file} --param /usr/src/pyradiomics/examples/exampleSettings/exampleMR_3mm.yaml --verbosity 5 --out ${output}/pyradiomics.csv --format-path basename --format csv"
pyradiomics ${batch_file} --param /usr/src/pyradiomics/examples/exampleSettings/exampleMR_3mm.yaml --verbosity 5 --out ${output}/pyradiomics.csv --format-path basename --format csv || die "pyradiomics failed"

echo

echo "Generating assessor"
echo "generate-pyradiomics-xml-from-csv $XNAT_HOST $project $session_id $session_label $scan_id $mask_file_uri ${output}/pyradiomics.csv ${output}/pyradiomics.xml"
generate-pyradiomics-xml-from-csv $XNAT_HOST $project $session_id $session_label $scan_id $mask_file_uri ${output}/pyradiomics.csv ${output}/pyradiomics.xml || die "failed to generate assessor"

echo
echo "All done"
